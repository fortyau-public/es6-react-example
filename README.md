# Example React Application

This is the application I'll be continuing to update over various lunch and learns
we will be doing based around React application architecture. I've tried to heavily
comment everything this application does, and will attempt to continue to do so.

## Setup App

1. Clone this repo
2. `npm install`

## Start Developing

1. `npm start`
2. Open this repo in your favorite code editor
3. Open `http://localhost:8080` in your favorite browser
4. Have fun!

## Make a build

1. `npm run build`
2. Look in that `dist` folder yo!
