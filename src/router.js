import React, { Component } from 'react'

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import Home from './pages/home'
import About from './pages/about'

export default class AppRouter extends Component {
  render() {
    return (
      <Router>
        <div>
          <ul>
            {/* Links change the current path, which will show a different route */}
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
          </ul>

          <hr />

          {/*  When paths match, component that is named will be loaded */}
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
        </div>
      </Router>
    )
  }
}
