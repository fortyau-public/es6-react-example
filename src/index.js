import React from 'react'
import { render } from 'react-dom'

import App from './app'

// Bootstrap the application to element with id of "app"
render(<App />, document.getElementById('app'))
