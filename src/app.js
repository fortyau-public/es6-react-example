import React, { Component } from 'react'
import Router from './router'

// Include styles at the app level, and leave it up to this styles.scss
// to import any other sass partials
require('./styles.scss')

export default class App extends Component {
  render() {
    return (
      <div>
        <Router />
      </div>
    )
  }
}
