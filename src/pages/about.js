import React, { Component } from 'react'

// The simplest a react component could ever be (pure fuction component)
export default function AboutPage() {
  return (<div>Im an about page</div>)
}
