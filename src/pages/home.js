import React, { Component } from 'react'
import Icon from 'react-icons/lib/fa/pied-piper-alt'

// An example of a basic component that implements changing state
export default class HomePage extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      catchphrase: 'Pied Piper'
    }
  }

  updateCatchphrase() {
    this.setState({ catchphrase: 'Middle out compression algorithm' })
  }

  render() {
    return (
      <div className="homepage">
        <Icon className="icon" />
        <h1>{this.state.catchphrase}</h1>

        <button onClick={::this.updateCatchphrase}>Change</button>
      </div>
    )
  }
}
