const path = require('path')

module.exports = {
  entry: [
    './src/index.js' // surprise! this should be your app's entry point
  ],
  output: {
    path: path.resolve(__dirname, './dist'), // folder to output bundle in
    filename: 'bundle.js' // name of bundle to output
  },
  module: {
    rules: [
      {
        test: /\.js/, // apply this rule to js files
        exclude: /node_modules/, // don't apply this rule to js files found in node_modules
        use: ['babel-loader'] // run this loader on all files that match above criteria
      },
      {
        test: /\.scss/, // apply this rule to scss files
        // loaders chain right to left. So, sass-loader's output goes to css-loaders, which goes to style-loader
        use: ['style-loader', 'css-loader','sass-loader']
      }
    ]
  }
}
